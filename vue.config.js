const path = require('path');
function resolveSrc(_path) {
    return path.join(__dirname, _path);
}
module.exports = {
    filenameHashing: false,
    configureWebpack: {
        devtool: 'source-map',
        entry: [
            './src/sass/style.sass',
            './src/main.js'
        ],
        output: {
            filename: '[name].[hash].js',
            path: path.resolve(__dirname, 'dist'),
        },
        resolve: {
            alias: {}
        },
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: [
                        resolveSrc('src'),
                    ],
                    loader: 'babel-loader'
                },
                {
                    test: /\.(graphql|gql)$/,
                    exclude: /node_modules/,
                    loader: 'graphql-tag/loader',
                },
            ],
        }
    },
    css: {
        sourceMap: process.env.NODE_ENV !== 'production',
        loaderOptions: {
            scss: {
                prependData: `
                    @import "src/sass/variables.sass";
                `
            },
            sass: {
                prependData: '@import src/sass/variables'
            }
        }
    }
};
